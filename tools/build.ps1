#!/usr/bin/env pwsh

param ([string]$command)
function PrepNexus {
    New-Item -ItemType directory -Path build/nexus/weapon_variation_expansion_vro -Force
}

function PrepSteam {
    New-Item -ItemType directory -Path build/steam/weapon_variation_expansion_vro -Force
}

function CleanNexus {
    Remove-Item -Path build/nexus -Force -Recurse -ErrorAction Ignore
}

function CleanSteam {
    Remove-Item -Path build/steam -Force -Recurse -ErrorAction Ignore
}

function BuildNexus {
    Write-Output "Building catalog and data files...."
    
    if ($IsLinux) {
        wine "$HOME/.steam/debian-installation/steamapps/common/X Tools/XRCatTool.exe" -in ./ -out ./build/nexus/weapon_variation_expansion_vro/ext_01.cat -exclude "^.git*" -exclude ".dae" -exclude "README.md" -exclude "content.xml" -exclude "tools/" -exclude "build/"
    } elseif ($IsWindows) {
        & "C:\Program Files(x86)\Steam\SteamApps\common\X Tools\XRCatTool.exe" -in ./ -out ./build/nexus/weapon_variation_expansion_vro/ext_01.cat -exclude "^.git*" -exclude ".dae" -exclude "README.md" -exclude "content.xml" -exclude "tools\" -exclude "build\"
    } else {
        ErrorOutput("Operating system is not supported by X4!")
    }
    if ($LASTEXITCODE -ne 0) { ErrorOutput("Build Failed") }
    Start-Sleep 1
    
    Write-Output "Copying content.xml into build dir..."
    Copy-Item -Path ./content.xml -Destination build/nexus/weapon_variation_expansion_vro/content.xml
    Set-Location -Path build/nexus
    Start-Sleep 1

    Write-Output "Compressing into zip file...."
    if ($IsLinux) {
        zip -r weapon_variation_expansion_vro.zip weapon_variation_expansion_vro/
    } elseif ($IsWindows) {
        & "C:\Program Files\7-Zip\7z.exe" a -tzip -o"weapon_variation_expansion_vro.zip" "weapon_variation_expansion_vro\" -aoa
    } else {
        ErrorOutput("Operating system is not supported by X4!")
    }

    Set-Location ../..

    Remove-Item build/nexus/weapon_variation_expansion_vro -Force -Recurse -ErrorAction Ignore

    Write-Output "Nexus Mods archive created!"
}

function BuildSteam {
    Write-Output "Building catalog and data files...."
    
    if ($IsLinux) {
        wine "$HOME/.steam/debian-installation/steamapps/common/X Tools/XRCatTool.exe" -in ./ -out ./build/steam/weapon_variation_expansion_vro/ext_01.cat -exclude "^.git*" -exclude ".dae" -exclude "README.md" -exclude "content.xml" -exclude "tools/" -exclude "build/"
    } elseif ($IsWindows) {
        & "C:\Program Files(x86)\Steam\SteamApps\common\X Tools\XRCatTool.exe" -in ./ -out ./build/steam/weapon_variation_expansion_vro/ext_01.cat -exclude "^.git*" -exclude ".dae" -exclude "README.md" -exclude "content.xml" -exclude "tools\" -exclude "build\"
    } else {
        ErrorOutput("Operating system is not supported by X4!")
    }
    if ($LASTEXITCODE -ne 0) { ErrorOutput("Build Failed") }
    Start-Sleep 1
    
    Write-Output "Copying content.xml into build dir..."
    Copy-Item -Path content.xml -Destination build/steam/weapon_variation_expansion_vro/content.xml
    
    Write-Output "Steam version ready for upload to the Workshop!"
}

function ErrorOutput([string]$message) {
    Write-Output $message
    exit 1
}

function Nexus {
    CleanNexus
    PrepNexus
    BuildNexus
}

function Steam {
    CleanSteam
    PrepSteam
    BuildSteam
}

function All {
    Nexus
    Steam
}

function Clean {
    Remove-Item build/* -Recurse -Force
}


switch ($command) {
    "steam" { Steam }
    "nexus" { Nexus }
    "all"   { All }
    "clean" { Clean }
    Default { ErrorOutput("Not a supported argument!") }
}